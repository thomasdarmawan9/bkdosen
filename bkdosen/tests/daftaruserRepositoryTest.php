<?php

use App\Models\daftaruser;
use App\Repositories\daftaruserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class daftaruserRepositoryTest extends TestCase
{
    use MakedaftaruserTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var daftaruserRepository
     */
    protected $daftaruserRepo;

    public function setUp()
    {
        parent::setUp();
        $this->daftaruserRepo = App::make(daftaruserRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatedaftaruser()
    {
        $daftaruser = $this->fakedaftaruserData();
        $createddaftaruser = $this->daftaruserRepo->create($daftaruser);
        $createddaftaruser = $createddaftaruser->toArray();
        $this->assertArrayHasKey('id', $createddaftaruser);
        $this->assertNotNull($createddaftaruser['id'], 'Created daftaruser must have id specified');
        $this->assertNotNull(daftaruser::find($createddaftaruser['id']), 'daftaruser with given id must be in DB');
        $this->assertModelData($daftaruser, $createddaftaruser);
    }

    /**
     * @test read
     */
    public function testReaddaftaruser()
    {
        $daftaruser = $this->makedaftaruser();
        $dbdaftaruser = $this->daftaruserRepo->find($daftaruser->id);
        $dbdaftaruser = $dbdaftaruser->toArray();
        $this->assertModelData($daftaruser->toArray(), $dbdaftaruser);
    }

    /**
     * @test update
     */
    public function testUpdatedaftaruser()
    {
        $daftaruser = $this->makedaftaruser();
        $fakedaftaruser = $this->fakedaftaruserData();
        $updateddaftaruser = $this->daftaruserRepo->update($fakedaftaruser, $daftaruser->id);
        $this->assertModelData($fakedaftaruser, $updateddaftaruser->toArray());
        $dbdaftaruser = $this->daftaruserRepo->find($daftaruser->id);
        $this->assertModelData($fakedaftaruser, $dbdaftaruser->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletedaftaruser()
    {
        $daftaruser = $this->makedaftaruser();
        $resp = $this->daftaruserRepo->delete($daftaruser->id);
        $this->assertTrue($resp);
        $this->assertNull(daftaruser::find($daftaruser->id), 'daftaruser should not exist in DB');
    }
}
