<?php

namespace App\Repositories;

use App\Models\tahun_ajar;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class tahun_ajarRepository
 * @package App\Repositories
 * @version May 10, 2019, 2:50 am UTC
 *
 * @method tahun_ajar findWithoutFail($id, $columns = ['*'])
 * @method tahun_ajar find($id, $columns = ['*'])
 * @method tahun_ajar first($columns = ['*'])
*/
class tahun_ajarRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tahun_ajar'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tahun_ajar::class;
    }
}
