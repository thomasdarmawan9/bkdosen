<?php

namespace App\Repositories;

use App\Models\users;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class usersRepository
 * @package App\Repositories
 * @version April 17, 2019, 2:31 am UTC
 *
 * @method users findWithoutFail($id, $columns = ['*'])
 * @method users find($id, $columns = ['*'])
 * @method users first($columns = ['*'])
*/
class usersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'NIK',
        'name',
        'email',
        'password',
        'jabatan',
        'prodi',
        'nohp'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return users::class;
    }
}
