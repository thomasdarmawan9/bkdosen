<?php

use Faker\Factory as Faker;
use App\Models\modul_rencana;
use App\Repositories\modul_rencanaRepository;

trait Makemodul_rencanaTrait
{
    /**
     * Create fake instance of modul_rencana and save it in database
     *
     * @param array $modulRencanaFields
     * @return modul_rencana
     */
    public function makemodul_rencana($modulRencanaFields = [])
    {
        /** @var modul_rencanaRepository $modulRencanaRepo */
        $modulRencanaRepo = App::make(modul_rencanaRepository::class);
        $theme = $this->fakemodul_rencanaData($modulRencanaFields);
        return $modulRencanaRepo->create($theme);
    }

    /**
     * Get fake instance of modul_rencana
     *
     * @param array $modulRencanaFields
     * @return modul_rencana
     */
    public function fakemodul_rencana($modulRencanaFields = [])
    {
        return new modul_rencana($this->fakemodul_rencanaData($modulRencanaFields));
    }

    /**
     * Get fake data of modul_rencana
     *
     * @param array $postFields
     * @return array
     */
    public function fakemodul_rencanaData($modulRencanaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'bidang' => $fake->randomElement(]),
            'kegiatan' => $fake->word,
            'sks' => $fake->word,
            'keterangan' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $modulRencanaFields);
    }
}
