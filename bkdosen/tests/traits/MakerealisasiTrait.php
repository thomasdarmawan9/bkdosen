<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\realisasi;
use App\Repositories\realisasiRepository;

trait MakerealisasiTrait
{
    /**
     * Create fake instance of realisasi and save it in database
     *
     * @param array $realisasiFields
     * @return realisasi
     */
    public function makerealisasi($realisasiFields = [])
    {
        /** @var realisasiRepository $realisasiRepo */
        $realisasiRepo = \App::make(realisasiRepository::class);
        $theme = $this->fakerealisasiData($realisasiFields);
        return $realisasiRepo->create($theme);
    }

    /**
     * Get fake instance of realisasi
     *
     * @param array $realisasiFields
     * @return realisasi
     */
    public function fakerealisasi($realisasiFields = [])
    {
        return new realisasi($this->fakerealisasiData($realisasiFields));
    }

    /**
     * Get fake data of realisasi
     *
     * @param array $realisasiFields
     * @return array
     */
    public function fakerealisasiData($realisasiFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_rencana_fk' => $fake->randomDigitNotNull,
            'realisasi_sks' => $fake->word,
            'bukti_pelaksanaan_sk' => $fake->word,
            'bukti_pelaksanaan_presensi' => $fake->word,
            'bukti_pelaksanaan_ppt' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $realisasiFields);
    }
}
