@if(Auth::user()->jabatan == 'Admin')
<li class="{{ Request::is('modulRencanas*') ? 'active' : '' }}">
    <a href="{!! route('modulRencanas.index') !!}"><i class="fa fa-edit"></i><span>Modul Rencana</span></a>
</li>


<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('modulVerifikasi*') ? 'active' : '' }}">
    <a href="/modulVerif"><i class="fa fa-edit"></i><span>Modul Verifikasi Rencana</span></a>
</li>
@endif

@if(Auth::user()->jabatan == 'Kaprodi')
<li class="{{ Request::is('modulRencanas*') ? 'active' : '' }}">
    <a href="{!! route('modulRencanas.index') !!}"><i class="fa fa-edit"></i><span>Modul Rencana</span></a>
</li>

<li class="{{ Request::is('modulVerifikasi*') ? 'active' : '' }}">
    <a href="/modulVerif"><i class="fa fa-edit"></i><span>Modul Verifikasi Rencana</span></a>
</li>
@endif

@if(Auth::user()->jabatan == 'Dosen')
<li class="{{ Request::is('modulRencanas*') ? 'active' : '' }}">
    <a href="{!! route('modulRencanas.index') !!}"><i class="fa fa-edit"></i><span>Modul Rencana</span></a>
</li>
@endif
<li class="{{ Request::is('tahunAjars*') ? 'active' : '' }}">
    <a href="{!! route('tahunAjars.index') !!}"><i class="fa fa-edit"></i><span>Tahun Ajars</span></a>
</li>

<li class="{{ Request::is('realisasis*') ? 'active' : '' }}">
    <a href="{!! route('realisasis.index') !!}"><i class="fa fa-edit"></i><span>Realisasis</span></a>
</li>

