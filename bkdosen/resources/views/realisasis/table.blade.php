<table class="table table-responsive" id="realisasis-table">
    <thead>
        <tr>
            <th>Id Rencana Fk</th>
            <th>Nama Kegiatan</th>
        <th>Realisasi Sks</th>
        <th>Bukti Pelaksanaan Sk</th>
        <th>Bukti Pelaksanaan Presensi</th>
        <th>Bukti Pelaksanaan Ppt</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($realisasis as $realisasi)
        <tr>
            <td>{!! $realisasi->id_rencana_fk !!}</td>

            <td>{!! $realisasi->rencana->nama_kegiatan !!}</td>
            
            <td>{!! $realisasi->realisasi_sks !!}</td>
            <td>{!! $realisasi->bukti_pelaksanaan_sk !!}</td>
            <td>{!! $realisasi->bukti_pelaksanaan_presensi !!}</td>
            <td>{!! $realisasi->bukti_pelaksanaan_ppt !!}</td>
            <td>
                {!! Form::open(['route' => ['realisasis.destroy', $realisasi->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('realisasis.show', [$realisasi->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('realisasis.edit', [$realisasi->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>