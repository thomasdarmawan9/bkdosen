<?php

namespace App\Repositories;

use App\Models\realisasi;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class realisasiRepository
 * @package App\Repositories
 * @version May 11, 2019, 7:53 am UTC
 *
 * @method realisasi findWithoutFail($id, $columns = ['*'])
 * @method realisasi find($id, $columns = ['*'])
 * @method realisasi first($columns = ['*'])
*/
class realisasiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_rencana_fk',
        'realisasi_sks',
        'bukti_pelaksanaan_sk',
        'bukti_pelaksanaan_presensi',
        'bukti_pelaksanaan_ppt'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return realisasi::class;
    }
}
