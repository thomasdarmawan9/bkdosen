<?php

use Illuminate\Database\Seeder;

class TahunAjarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tahun_ajars')->insert([
            'tahun_ajar' => '2016/2017'
        ]);
        DB::table('tahun_ajars')->insert([
            'tahun_ajar' => '2017/2018'
        ]);
        DB::table('tahun_ajars')->insert([
            'tahun_ajar' => '2018/2019'
        ]);
        DB::table('tahun_ajars')->insert([
            'tahun_ajar' => '2019/2020'
        ]);
    }
}
