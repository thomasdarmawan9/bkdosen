<?php

use Faker\Factory as Faker;
use App\Models\daftaruser;
use App\Repositories\daftaruserRepository;

trait MakedaftaruserTrait
{
    /**
     * Create fake instance of daftaruser and save it in database
     *
     * @param array $daftaruserFields
     * @return daftaruser
     */
    public function makedaftaruser($daftaruserFields = [])
    {
        /** @var daftaruserRepository $daftaruserRepo */
        $daftaruserRepo = App::make(daftaruserRepository::class);
        $theme = $this->fakedaftaruserData($daftaruserFields);
        return $daftaruserRepo->create($theme);
    }

    /**
     * Get fake instance of daftaruser
     *
     * @param array $daftaruserFields
     * @return daftaruser
     */
    public function fakedaftaruser($daftaruserFields = [])
    {
        return new daftaruser($this->fakedaftaruserData($daftaruserFields));
    }

    /**
     * Get fake data of daftaruser
     *
     * @param array $postFields
     * @return array
     */
    public function fakedaftaruserData($daftaruserFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'NIK' => $fake->word,
            'password' => $fake->word,
            'nama' => $fake->word,
            'prodi' => $fake->word,
            'nohp' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $daftaruserFields);
    }
}
