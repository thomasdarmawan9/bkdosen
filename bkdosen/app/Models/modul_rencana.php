<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class modul_rencana
 * @package App\Models
 * @version March 7, 2019, 10:39 am UTC
 *
 * @property enum bidang
 * @property string kegiatan
 * @property string sks
 * @property string keterangan
 */
class modul_rencana extends Model
{
    use SoftDeletes;

    public $table = 'modul_rencanas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [

        'NIK',
        'tahun_ajar',
        'prodi_fk',
        'bidang',
        'sub_bidang',
        'nama_kegiatan',
        'rencana_sks',
        'keterangan',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

        'NIK' => 'string',
        'tahun_ajar' => 'string',
        'prodi_fk' => 'sting',
        'bidang' => 'string',
        'sub_bidang' => 'string',
        'nama_kegiatan' => 'string',
        'rencana_sks' => 'string',
        'keterangan' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

        'NIK' => 'required',
        'tahun_ajar' => 'required',
        'prodi_fk' => 'required',
        'bidang' => 'required',
        'sub_bidang' => 'required',
        'nama_kegiatan' => 'required',
        'rencana_sks' => 'numeric',
        'keterangan' => '',
        'status' => 'required'
    ];

    public function realisasi(){
    	return $this->hasMany('\App\Models\realisasi','id_rencana_fk');
    }
    
    
}
