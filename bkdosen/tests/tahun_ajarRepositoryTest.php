<?php namespace Tests\Repositories;

use App\Models\tahun_ajar;
use App\Repositories\tahun_ajarRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\Maketahun_ajarTrait;
use Tests\ApiTestTrait;

class tahun_ajarRepositoryTest extends TestCase
{
    use Maketahun_ajarTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var tahun_ajarRepository
     */
    protected $tahunAjarRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tahunAjarRepo = \App::make(tahun_ajarRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tahun_ajar()
    {
        $tahunAjar = $this->faketahun_ajarData();
        $createdtahun_ajar = $this->tahunAjarRepo->create($tahunAjar);
        $createdtahun_ajar = $createdtahun_ajar->toArray();
        $this->assertArrayHasKey('id', $createdtahun_ajar);
        $this->assertNotNull($createdtahun_ajar['id'], 'Created tahun_ajar must have id specified');
        $this->assertNotNull(tahun_ajar::find($createdtahun_ajar['id']), 'tahun_ajar with given id must be in DB');
        $this->assertModelData($tahunAjar, $createdtahun_ajar);
    }

    /**
     * @test read
     */
    public function test_read_tahun_ajar()
    {
        $tahunAjar = $this->maketahun_ajar();
        $dbtahun_ajar = $this->tahunAjarRepo->find($tahunAjar->id);
        $dbtahun_ajar = $dbtahun_ajar->toArray();
        $this->assertModelData($tahunAjar->toArray(), $dbtahun_ajar);
    }

    /**
     * @test update
     */
    public function test_update_tahun_ajar()
    {
        $tahunAjar = $this->maketahun_ajar();
        $faketahun_ajar = $this->faketahun_ajarData();
        $updatedtahun_ajar = $this->tahunAjarRepo->update($faketahun_ajar, $tahunAjar->id);
        $this->assertModelData($faketahun_ajar, $updatedtahun_ajar->toArray());
        $dbtahun_ajar = $this->tahunAjarRepo->find($tahunAjar->id);
        $this->assertModelData($faketahun_ajar, $dbtahun_ajar->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tahun_ajar()
    {
        $tahunAjar = $this->maketahun_ajar();
        $resp = $this->tahunAjarRepo->delete($tahunAjar->id);
        $this->assertTrue($resp);
        $this->assertNull(tahun_ajar::find($tahunAjar->id), 'tahun_ajar should not exist in DB');
    }
}
