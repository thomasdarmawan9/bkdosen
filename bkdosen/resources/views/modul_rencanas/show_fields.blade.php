<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $modulRencana->id !!}</p>
</div>

<!-- Bidang Field -->
<div class="form-group">
    {!! Form::label('bidang', 'Bidang:') !!}
    <p>{!! $modulRencana->bidang !!}</p>
</div>

<!-- Kegiatan Field -->
<div class="form-group">
    {!! Form::label('kegiatan', 'Kegiatan:') !!}
    <p>{!! $modulRencana->nama_kegiatan !!}</p>
</div>

<!-- Sks Field -->
<div class="form-group">
    {!! Form::label('sks', 'Sks:') !!}
    <p>{!! $modulRencana->rencana_sks !!}</p>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{!! $modulRencana->keterangan !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $modulRencana->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $modulRencana->updated_at !!}</p>
</div>

