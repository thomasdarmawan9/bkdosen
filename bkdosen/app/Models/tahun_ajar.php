<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class tahun_ajar
 * @package App\Models
 * @version May 10, 2019, 2:50 am UTC
 *
 * @property string tahun_ajar
 */
class tahun_ajar extends Model
{
    use SoftDeletes;

    public $table = 'tahun_ajars';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'tahun_ajar'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tahun_ajar' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tahun_ajar' => 'required'
    ];

    
}
