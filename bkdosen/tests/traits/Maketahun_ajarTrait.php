<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\tahun_ajar;
use App\Repositories\tahun_ajarRepository;

trait Maketahun_ajarTrait
{
    /**
     * Create fake instance of tahun_ajar and save it in database
     *
     * @param array $tahunAjarFields
     * @return tahun_ajar
     */
    public function maketahun_ajar($tahunAjarFields = [])
    {
        /** @var tahun_ajarRepository $tahunAjarRepo */
        $tahunAjarRepo = \App::make(tahun_ajarRepository::class);
        $theme = $this->faketahun_ajarData($tahunAjarFields);
        return $tahunAjarRepo->create($theme);
    }

    /**
     * Get fake instance of tahun_ajar
     *
     * @param array $tahunAjarFields
     * @return tahun_ajar
     */
    public function faketahun_ajar($tahunAjarFields = [])
    {
        return new tahun_ajar($this->faketahun_ajarData($tahunAjarFields));
    }

    /**
     * Get fake data of tahun_ajar
     *
     * @param array $tahunAjarFields
     * @return array
     */
    public function faketahun_ajarData($tahunAjarFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'tahun_ajar' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $tahunAjarFields);
    }
}
