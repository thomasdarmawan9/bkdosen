
<div class="form-group col-sm-6">
    {!! Form::label('tahun_ajar', 'tahun ajar:') !!}
    {!! Form::text('tahun_ajar', null , ['class' => 'form-control']) !!}
 
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tahunAjars.index') !!}" class="btn btn-default">Cancel</a>
</div>
