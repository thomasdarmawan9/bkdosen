<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createtahun_ajarAPIRequest;
use App\Http\Requests\API\Updatetahun_ajarAPIRequest;
use App\Models\tahun_ajar;
use App\Repositories\tahun_ajarRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class tahun_ajarController
 * @package App\Http\Controllers\API
 */

class tahun_ajarAPIController extends AppBaseController
{
    /** @var  tahun_ajarRepository */
    private $tahunAjarRepository;

    public function __construct(tahun_ajarRepository $tahunAjarRepo)
    {
        $this->tahunAjarRepository = $tahunAjarRepo;
    }

    /**
     * Display a listing of the tahun_ajar.
     * GET|HEAD /tahunAjars
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tahunAjarRepository->pushCriteria(new RequestCriteria($request));
        $this->tahunAjarRepository->pushCriteria(new LimitOffsetCriteria($request));
        $tahunAjars = $this->tahunAjarRepository->all();

        return $this->sendResponse($tahunAjars->toArray(), 'Tahun Ajars retrieved successfully');
    }

    /**
     * Store a newly created tahun_ajar in storage.
     * POST /tahunAjars
     *
     * @param Createtahun_ajarAPIRequest $request
     *
     * @return Response
     */
    public function store(Createtahun_ajarAPIRequest $request)
    {
        $input = $request->all();

        $tahunAjar = $this->tahunAjarRepository->create($input);

        return $this->sendResponse($tahunAjar->toArray(), 'Tahun Ajar saved successfully');
    }

    /**
     * Display the specified tahun_ajar.
     * GET|HEAD /tahunAjars/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var tahun_ajar $tahunAjar */
        $tahunAjar = $this->tahunAjarRepository->findWithoutFail($id);

        if (empty($tahunAjar)) {
            return $this->sendError('Tahun Ajar not found');
        }

        return $this->sendResponse($tahunAjar->toArray(), 'Tahun Ajar retrieved successfully');
    }

    /**
     * Update the specified tahun_ajar in storage.
     * PUT/PATCH /tahunAjars/{id}
     *
     * @param  int $id
     * @param Updatetahun_ajarAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetahun_ajarAPIRequest $request)
    {
        $input = $request->all();

        /** @var tahun_ajar $tahunAjar */
        $tahunAjar = $this->tahunAjarRepository->findWithoutFail($id);

        if (empty($tahunAjar)) {
            return $this->sendError('Tahun Ajar not found');
        }

        $tahunAjar = $this->tahunAjarRepository->update($input, $id);

        return $this->sendResponse($tahunAjar->toArray(), 'tahun_ajar updated successfully');
    }

    /**
     * Remove the specified tahun_ajar from storage.
     * DELETE /tahunAjars/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var tahun_ajar $tahunAjar */
        $tahunAjar = $this->tahunAjarRepository->findWithoutFail($id);

        if (empty($tahunAjar)) {
            return $this->sendError('Tahun Ajar not found');
        }

        $tahunAjar->delete();

        return $this->sendResponse($id, 'Tahun Ajar deleted successfully');
    }
}
