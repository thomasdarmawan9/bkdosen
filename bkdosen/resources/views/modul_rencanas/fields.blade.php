<!-- Bidang Field -->
<div class="form-group col-sm-6">
    {!! Form::label('NIK', 'NIK:') !!}
    {!! Form::text('NIK', Auth::user()->NIK , ['class' => 'form-control']) !!}
 
</div>

<div class="form-group col-sm-6">
    {!! Form::label('Prodi', 'Prodi:') !!}
    {!! Form::text('prodi_fk', Auth::user()->prodi , ['class' => 'form-control']) !!}
 
</div>


<!-- Tahun Ajar Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tahun_ajar', 'Tahun Ajar:') !!}
    {!! Form::select('tahun_ajar', $tahun_ajar , null,  ['class' => 'form-control']) !!}
</div>


      
<!-- Bidang Field -->
<div class="col-md-6 col-sm-6" style="margin-bottom:15px;">
{!! Form::label('bidang', 'Bidang:') !!}
<select class="form-control" id="bidang" name="bidang">
<option value="">Pilih Bidang</option>
<option value="Pendidikan/Pengajar">Pendidikan/Pengajar</option>
<option value="BidangPeneltian">Bidang Penelitian</option>
<option value="BidangPengmas">Bidang Pengabdian pada Masyarakat</option>
<option value="BidangPenunjang">Bidang Penunjang</option>
</select>
</div>
<!-- Sub Bidang Field -->
<div class="col-md-6 col-sm-6">
{!! Form::label('sub_bidang', 'Sub Bidang:') !!}
<select class="form-control" id="sub_bidang" name="sub_bidang">
<option value="">Pilih Sub Bidang</option>
<option value="Pendidikan/Pengajar" data-chained="Pendidikan/Pengajar">Perkuliahan</option>
<option value="Praktikum" data-chained="Pendidikan/Pengajar">Praktikum</option>
<option value="Bimbingan Praktek Kerja/Magang" data-chained="Pendidikan/Pengajar">Bimbingan Praktek Kerja/Magang</option>
<option value="Seminar Terjadwal/Seminar KP" data-chained="Pendidikan/Pengajar">Seminar Terjadwal/Seminar KP</option>
<option value="Bimbingan Tugas Akhir/Skripsi/PKM" data-chained="Pendidikan/Pengajar">Bimbingan Tugas Akhir/Skripsi/PKM</option>
<option value="Menguji Tugas Akhir/Skripsi" data-chained="Pendidikan/Pengajar">Menguji Tugas Akhir/Skripsi</option>
<option value="Menguji Proposal Skripsi" data-chained="Pendidikan/Pengajar">Menguji Proposal Skripsi</option>
<option value="Membimbing Dosen Muda" data-chained="Pendidikan/Pengajar">Membimbing Dosen Muda</option>
<option value="Datasering/pencangkokan dosen" data-chained="Pendidikan/Pengajar">Datasering/pencangkokan dosen</option>
<option value="Mengembangkan Program Pengajaran" data-chained="Pendidikan/Pengajar">Mengembangkan Program Pengajaran</option>

<option value="Melakukan Penelitian Kelompok" data-chained="BidangPeneltian">Melakukan Penelitian Kelompok</option>
<option value="Melakukan Penelitian Mandiri" data-chained="BidangPeneltian">Melakukan Penelitian Mandiri</option>
<option value="Menulis Buku" data-chained="BidangPeneltian">Menulis Buku</option>
<option value="Menterjemahkan Buku" data-chained="BidangPeneltian">Menterjemahkan Buku</option>
<option value="Menyunting Buku" data-chained="BidangPeneltian">Menyunting Buku</option>
<option value="Menulis Diktat/Modul" data-chained="BidangPeneltian">Menulis Diktat/Modul</option>
<option value="Sebagai Asesor BKD" data-chained="BidangPeneltian">Sebagai Asesor BKD</option>
<option value="Menulis Jurnal Ilmiah/Poster/Proceeding" data-chained="BidangPeneltian">Menulis Jurnal Ilmiah/Poster/Proceeding</option>
<option value="Memperoleh Hak Paten" data-chained="BidangPeneltian">Memperoleh Hak Paten</option>
<option value="Menulis di Media Massa sesuai bidang" data-chained="BidangPeneltian">Menulis di Media Massa sesuai bidang</option>
<option value="Menyampaikan Orasi Ilmiah" data-chained="BidangPeneltian">Menyampaikan Orasi Ilmiah</option>
<option value="Penyaji Makalah dalam Seminar" data-chained="BidangPeneltian">Penyaji Makalah dalam Seminar</option>

<option value="Kegiatan Pengabdian pada Masyarakat" data-chained="BidangPengmas">Kegiatan Pengabdian pada Masyarakat</option>
<option value="Memberikan penyuluhan/penataran/ konsultansi pada masyarakat" data-chained="BidangPengmas">Memberikan penyuluhan/penataran/ konsultansi pada masyarakat</option>
<option value="Memberikan jasa konsultansi sebagai tenaga ahli" data-chained="BidangPengmas">Memberikan jasa konsultansi sebagai tenaga ahli</option>
<option value="Membuat karya pengabdian masyarakat yang digunakan untuk siswa SMA/umum" data-chained="BidangPengmas">Membuat karya pengabdian masyarakat yang digunakan untuk siswa SMA/umum</option>
<option value="Membuat karya pengabdian masyarakat sebagai modul/panduan yang digunakan oleh mahasiswa" data-chained="BidangPengmas">Membuat karya pengmas sebagai modul yang digunakan oleh mahasiswa</option>

<option value="Pembimbingan Akademik bagi Mahasiswa" data-chained="BidangPenunjang">Pembimbingan Akademik bagi Mahasiswa</option>
<option value="Bimbingan dan Konseling" data-chained="BidangPenunjang">Bimbingan dan Konseling</option>
<option value="Pembina unit kegiatan mahasiswa" data-chained="BidangPenunjang">Pembina unit kegiatan mahasiswa</option>
<option value="Jabatan Struktural/Non Struktural" data-chained="BidangPenunjang">Jabatan Struktural/Non Struktural</option>
<option value="Redaksi Jurnal ber-ISSN" data-chained="BidangPenunjang">Redaksi Jurnal ber-ISSN</option>
<option value="Panitia AdHoc/Panitia Tetap" data-chained="BidangPenunjang">Panitia AdHoc/Panitia Tetap</option>
<option value="Pengurus/Anggota Asosiasi Profesi" data-chained="BidangPenunjang">Pengurus/Anggota Asosiasi Profesi</option>
<option value="Peserta/juri/moderator dalam seminar/workshop/pelatihan" data-chained="BidangPenunjang">Peserta/juri/moderator dalam seminar/workshop/pelatihan</option>
<option value="Reviewer Jurnal/Proposal/Buku" data-chained="BidangPenunjang">Reviewer Jurnal/Proposal/Buku</option>
<option value="Kegiatan internal UPJ" data-chained="BidangPenunjang">Kegiatan internal UPJ</option>
</select>
</div>


<!-- Nama Kegiatan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_kegiatan', 'Nama Kegiatan:') !!}
    {!! Form::text('nama_kegiatan', null, ['class' => 'form-control']) !!}
</div>

<!-- Rencana Sks Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rencana_sks', 'Rencana Sks:') !!}
    {!! Form::number('rencana_sks', null, ['class' => 'form-control']) !!}
</div>

<!-- Keterangan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
</div>
<!-- Status Field -->
<div class="form-group col-sm-6">
        {!!  Form::hidden('status', 'Not Verified') !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('modulRencanas.index') !!}" class="btn btn-default">Cancel</a>
</div>
