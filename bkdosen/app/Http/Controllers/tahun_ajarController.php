<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createtahun_ajarRequest;
use App\Http\Requests\Updatetahun_ajarRequest;
use App\Repositories\tahun_ajarRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class tahun_ajarController extends AppBaseController
{
    /** @var  tahun_ajarRepository */
    private $tahunAjarRepository;

    public function __construct(tahun_ajarRepository $tahunAjarRepo)
    {
        $this->tahunAjarRepository = $tahunAjarRepo;
    }

    /**
     * Display a listing of the tahun_ajar.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tahunAjarRepository->pushCriteria(new RequestCriteria($request));
        $tahunAjars = $this->tahunAjarRepository->all();

        return view('tahun_ajars.index')
            ->with('tahunAjars', $tahunAjars);
    }

    /**
     * Show the form for creating a new tahun_ajar.
     *
     * @return Response
     */
    public function create()
    {
        return view('tahun_ajars.create');
    }

    /**
     * Store a newly created tahun_ajar in storage.
     *
     * @param Createtahun_ajarRequest $request
     *
     * @return Response
     */
    public function store(Createtahun_ajarRequest $request)
    {
        $input = $request->all();

        $tahunAjar = $this->tahunAjarRepository->create($input);

        Flash::success('Tahun Ajar saved successfully.');

        return redirect(route('tahunAjars.index'));
    }

    /**
     * Display the specified tahun_ajar.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tahunAjar = $this->tahunAjarRepository->findWithoutFail($id);

        if (empty($tahunAjar)) {
            Flash::error('Tahun Ajar not found');

            return redirect(route('tahunAjars.index'));
        }

        return view('tahun_ajars.show')->with('tahunAjar', $tahunAjar);
    }

    /**
     * Show the form for editing the specified tahun_ajar.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tahunAjar = $this->tahunAjarRepository->findWithoutFail($id);

        if (empty($tahunAjar)) {
            Flash::error('Tahun Ajar not found');

            return redirect(route('tahunAjars.index'));
        }

        return view('tahun_ajars.edit')->with('tahunAjar', $tahunAjar);
    }

    /**
     * Update the specified tahun_ajar in storage.
     *
     * @param  int              $id
     * @param Updatetahun_ajarRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetahun_ajarRequest $request)
    {
        $tahunAjar = $this->tahunAjarRepository->findWithoutFail($id);

        if (empty($tahunAjar)) {
            Flash::error('Tahun Ajar not found');

            return redirect(route('tahunAjars.index'));
        }

        $tahunAjar = $this->tahunAjarRepository->update($request->all(), $id);

        Flash::success('Tahun Ajar updated successfully.');

        return redirect(route('tahunAjars.index'));
    }

    /**
     * Remove the specified tahun_ajar from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tahunAjar = $this->tahunAjarRepository->findWithoutFail($id);

        if (empty($tahunAjar)) {
            Flash::error('Tahun Ajar not found');

            return redirect(route('tahunAjars.index'));
        }

        $this->tahunAjarRepository->delete($id);

        Flash::success('Tahun Ajar deleted successfully.');

        return redirect(route('tahunAjars.index'));
    }
}
