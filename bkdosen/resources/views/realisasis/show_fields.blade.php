
<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $realisasi->id !!}</p>
</div>

<!-- Id Rencana Fk Field -->
<div class="form-group">
    {!! Form::label('id_rencana_fk', 'Id Rencana Fk:') !!}
    <p>{!! $realisasi->id_rencana_fk !!}</p>
</div>

<!-- Realisasi Sks Field -->
<div class="form-group">
    {!! Form::label('realisasi_sks', 'Realisasi Sks:') !!}
    <p>{!! $realisasi->realisasi_sks !!}</p>
</div>

<!-- Bukti Pelaksanaan Sk Field -->
<div class="form-group">
    {!! Form::label('bukti_pelaksanaan_sk', 'Bukti Pelaksanaan Sk:') !!}
    <p>{!! $realisasi->bukti_pelaksanaan_sk !!}</p>
</div>

<!-- Bukti Pelaksanaan Presensi Field -->
<div class="form-group">
    {!! Form::label('bukti_pelaksanaan_presensi', 'Bukti Pelaksanaan Presensi:') !!}
    <p>{!! $realisasi->bukti_pelaksanaan_presensi !!}</p>
</div>

<!-- Bukti Pelaksanaan Ppt Field -->
<div class="form-group">
    {!! Form::label('bukti_pelaksanaan_ppt', 'Bukti Pelaksanaan Ppt:') !!}
    <p>{!! $realisasi->bukti_pelaksanaan_ppt !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $realisasi->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $realisasi->updated_at !!}</p>
</div>

