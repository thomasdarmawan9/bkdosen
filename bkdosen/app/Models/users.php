<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class users
 * @package App\Models
 * @version April 17, 2019, 2:31 am UTC
 *
 * @property string NIK
 * @property string name
 * @property string email
 * @property string password
 * @property string jabatan
 * @property string prodi
 * @property string nohp
 */
class users extends Model
{
    public $table = 'users';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'NIK',
        'name',
        'email',
        'password',
        'jabatan',
        'prodi',
        'nohp'
    ];

   

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'NIK' => 'string',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'jabatan' => 'string',
        'prodi' => 'string',
        'nohp' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'NIK' => 'required',
        'name' => 'required',
        'email' => 'email',
        'password' => 'required',
        'jabatan' => 'required',
        'prodi' => '',
        'nohp' => 'required'
    ];

    
}
