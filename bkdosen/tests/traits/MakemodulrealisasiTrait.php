<?php

use Faker\Factory as Faker;
use App\Models\modulrealisasi;
use App\Repositories\modulrealisasiRepository;

trait MakemodulrealisasiTrait
{
    /**
     * Create fake instance of modulrealisasi and save it in database
     *
     * @param array $modulrealisasiFields
     * @return modulrealisasi
     */
    public function makemodulrealisasi($modulrealisasiFields = [])
    {
        /** @var modulrealisasiRepository $modulrealisasiRepo */
        $modulrealisasiRepo = App::make(modulrealisasiRepository::class);
        $theme = $this->fakemodulrealisasiData($modulrealisasiFields);
        return $modulrealisasiRepo->create($theme);
    }

    /**
     * Get fake instance of modulrealisasi
     *
     * @param array $modulrealisasiFields
     * @return modulrealisasi
     */
    public function fakemodulrealisasi($modulrealisasiFields = [])
    {
        return new modulrealisasi($this->fakemodulrealisasiData($modulrealisasiFields));
    }

    /**
     * Get fake data of modulrealisasi
     *
     * @param array $postFields
     * @return array
     */
    public function fakemodulrealisasiData($modulrealisasiFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'bidang' => $fake->word,
            'subbidang' => $fake->word,
            'kegiatan' => $fake->word,
            'rencana' => $fake->word,
            'realisasi' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $modulrealisasiFields);
    }
}
