<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class realisasi
 * @package App\Models
 * @version May 11, 2019, 7:53 am UTC
 *
 * @property integer id_rencana_fk
 * @property string realisasi_sks
 * @property string bukti_pelaksanaan_sk
 * @property string bukti_pelaksanaan_presensi
 * @property string bukti_pelaksanaan_ppt
 */
class realisasi extends Model
{
    use SoftDeletes;

    public $table = 'realisasis';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_rencana_fk',
        'realisasi_sks',
        'bukti_pelaksanaan_sk',
        'bukti_pelaksanaan_presensi',
        'bukti_pelaksanaan_ppt'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_rencana_fk' => 'integer',
        'realisasi_sks' => 'string',
        'bukti_pelaksanaan_sk' => 'string',
        'bukti_pelaksanaan_presensi' => 'string',
        'bukti_pelaksanaan_ppt' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_rencana_fk' => 'required'
    ];

    public function rencana(){
    	return $this->hasOne('\App\Models\modul_rencana','id','id_rencana_fk');
    }
}
