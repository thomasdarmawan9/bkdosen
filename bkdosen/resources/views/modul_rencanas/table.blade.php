
<table class="table table-responsive" id="modulRencanas-table">
    <thead>
        <tr>
            <th>Tahun Ajar</th>
            <th>Bidang</th>
            <th>Sub Bidang</th>
            <th>Nama Kegiatan</th>
            <th>Rencana Sks</th>
            <th>Keterangan</th>
            <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($modulRencanas as $modulRencana)
        <tr>
            <td>{!! $modulRencana->tahun_ajar !!}</td>
            <td>{!! $modulRencana->bidang !!}</td>
            <td>{!! $modulRencana->sub_bidang !!}</td>
            <td>{!! $modulRencana->nama_kegiatan !!}</td>
            <td>{!! $modulRencana->rencana_sks !!}</td>
            <td>{!! $modulRencana->keterangan !!}</td>
            <td>{!! $modulRencana->status !!}</td>
            <td>
                {!! Form::open(['route' => ['modulRencanas.destroy', $modulRencana->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('modulRencanas.show', [$modulRencana->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('modulRencanas.edit', [$modulRencana->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>