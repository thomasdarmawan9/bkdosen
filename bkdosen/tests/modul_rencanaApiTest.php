<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class modul_rencanaApiTest extends TestCase
{
    use Makemodul_rencanaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatemodul_rencana()
    {
        $modulRencana = $this->fakemodul_rencanaData();
        $this->json('POST', '/api/v1/modulRencanas', $modulRencana);

        $this->assertApiResponse($modulRencana);
    }

    /**
     * @test
     */
    public function testReadmodul_rencana()
    {
        $modulRencana = $this->makemodul_rencana();
        $this->json('GET', '/api/v1/modulRencanas/'.$modulRencana->id);

        $this->assertApiResponse($modulRencana->toArray());
    }

    /**
     * @test
     */
    public function testUpdatemodul_rencana()
    {
        $modulRencana = $this->makemodul_rencana();
        $editedmodul_rencana = $this->fakemodul_rencanaData();

        $this->json('PUT', '/api/v1/modulRencanas/'.$modulRencana->id, $editedmodul_rencana);

        $this->assertApiResponse($editedmodul_rencana);
    }

    /**
     * @test
     */
    public function testDeletemodul_rencana()
    {
        $modulRencana = $this->makemodul_rencana();
        $this->json('DELETE', '/api/v1/modulRencanas/'.$modulRencana->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/modulRencanas/'.$modulRencana->id);

        $this->assertResponseStatus(404);
    }
}
