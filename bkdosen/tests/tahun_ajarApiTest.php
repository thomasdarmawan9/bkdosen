<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\Maketahun_ajarTrait;
use Tests\ApiTestTrait;

class tahun_ajarApiTest extends TestCase
{
    use Maketahun_ajarTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tahun_ajar()
    {
        $tahunAjar = $this->faketahun_ajarData();
        $this->response = $this->json('POST', '/api/tahunAjars', $tahunAjar);

        $this->assertApiResponse($tahunAjar);
    }

    /**
     * @test
     */
    public function test_read_tahun_ajar()
    {
        $tahunAjar = $this->maketahun_ajar();
        $this->response = $this->json('GET', '/api/tahunAjars/'.$tahunAjar->id);

        $this->assertApiResponse($tahunAjar->toArray());
    }

    /**
     * @test
     */
    public function test_update_tahun_ajar()
    {
        $tahunAjar = $this->maketahun_ajar();
        $editedtahun_ajar = $this->faketahun_ajarData();

        $this->response = $this->json('PUT', '/api/tahunAjars/'.$tahunAjar->id, $editedtahun_ajar);

        $this->assertApiResponse($editedtahun_ajar);
    }

    /**
     * @test
     */
    public function test_delete_tahun_ajar()
    {
        $tahunAjar = $this->maketahun_ajar();
        $this->response = $this->json('DELETE', '/api/tahunAjars/'.$tahunAjar->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/tahunAjars/'.$tahunAjar->id);

        $this->response->assertStatus(404);
    }
}
