<table class="table table-responsive" id="tahunAjars-table">
    <thead>
        <tr>
            <th>Tahun Ajar</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tahunAjars as $tahunAjar)
        <tr>
            <td>{!! $tahunAjar->tahun_ajar !!}</td>
            <td>
                {!! Form::open(['route' => ['tahunAjars.destroy', $tahunAjar->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tahunAjars.show', [$tahunAjar->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tahunAjars.edit', [$tahunAjar->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>