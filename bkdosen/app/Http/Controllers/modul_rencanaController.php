<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createmodul_rencanaRequest;
use App\Http\Requests\Updatemodul_rencanaRequest;
use App\Repositories\modul_rencanaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class modul_rencanaController extends AppBaseController
{
    /** @var  modul_rencanaRepository */
    private $modulRencanaRepository;

    public function __construct(modul_rencanaRepository $modulRencanaRepo)
    {
        $this->modulRencanaRepository = $modulRencanaRepo;
    }

    /**
     * Display a listing of the modul_rencana.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->modulRencanaRepository->pushCriteria(new RequestCriteria($request));
        $modulRencanas = \App\Models\modul_rencana::where('NIK', '=', \Auth::user()->NIK)->get();
        
        return view('modul_rencanas.index')
            ->with('modulRencanas', $modulRencanas);
    }

    public function index2(Request $request)
    {
        $this->modulRencanaRepository->pushCriteria(new RequestCriteria($request));
        $modulRencanas = \App\Models\modul_rencana::where('prodi_fk', '=', \Auth::user()->prodi)->get();

        return view('modul_rencanas.index2')
            ->with('modulRencanas', $modulRencanas);
    }
    /**
     * Show the form for creating a new modul_rencana.
     *
     * @return Response
     */
    public function create()
    {   
        $tahun_ajar = DB::table('tahun_ajars')->pluck('tahun_ajar','tahun_ajar');

        return view('modul_rencanas.create', compact('tahun_ajar','tahun_ajar'));
    }

    /**
     * Store a newly created modul_rencana in storage.
     *
     * @param Createmodul_rencanaRequest $request
     *
     * @return Response
     */
    public function store(Createmodul_rencanaRequest $request)
    {
        $input = $request->all();

        $modulRencana = $this->modulRencanaRepository->create($input);

        Flash::success('Modul Rencana saved successfully.');

        return redirect(route('modulRencanas.index'));
    }

    /**
     * Display the specified modul_rencana.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $modulRencana = $this->modulRencanaRepository->findWithoutFail($id);

        if (empty($modulRencana)) {
            Flash::error('Modul Rencana not found');

            return redirect(route('modulRencanas.index'));
        }

        return view('modul_rencanas.show')->with('modulRencana', $modulRencana);
    }

    /**
     * Show the form for editing the specified modul_rencana.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $modulRencana = $this->modulRencanaRepository->findWithoutFail($id);

        $tahun_ajar = DB::table('tahun_ajars')->pluck('tahun_ajar','tahun_ajar');


        if (empty($modulRencana)) {

          
            Flash::error('Modul Rencana not found');

            return redirect(route('modulRencanas.index'));
        }

        return view('modul_rencanas.edit',  compact('tahun_ajar','tahun_ajar'))->with('modulRencana', $modulRencana);

    }

    /**
     * Update the specified modul_rencana in storage.
     *
     * @param  int              $id
     * @param Updatemodul_rencanaRequest $request
     *
     * @return Response
     */
    public function update($id, Updatemodul_rencanaRequest $request)
    {
        $modulRencana = $this->modulRencanaRepository->findWithoutFail($id);

        if (empty($modulRencana)) {
            Flash::error('Modul Rencana not found');

            return redirect(route('modulRencanas.index'));
        }

        $modulRencana = $this->modulRencanaRepository->update($request->all(), $id);

        Flash::success('Modul Rencana updated successfully.');

        return redirect(route('modulRencanas.index'));
    }

    /**
     * Remove the specified modul_rencana from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $modulRencana = $this->modulRencanaRepository->findWithoutFail($id);

        if (empty($modulRencana)) {
            Flash::error('Modul Rencana not found');

            return redirect(route('modulRencanas.index'));
        }

        $this->modulRencanaRepository->delete($id);

        Flash::success('Modul Rencana deleted successfully.');

        return redirect(route('modulRencanas.index'));
    }
    public function verifikasi($id){

        DB::table('modul_rencanas')
            ->where('id', $id)
            ->update(['status' => "Verified"]);
            return redirect('/modulVerif');
    }
}
