<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $users->id !!}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('NIK', 'Nik:') !!}
    <p>{!! $users->NIK !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $users->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $users->email !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $users->password !!}</p>
</div>

<!-- Jabatan Field -->
<div class="form-group">
    {!! Form::label('jabatan', 'Jabatan:') !!}
    <p>{!! $users->jabatan !!}</p>
</div>

<!-- Prodi Field -->
<div class="form-group">
    {!! Form::label('prodi', 'Prodi:') !!}
    <p>{!! $users->prodi !!}</p>
</div>

<!-- Nohp Field -->
<div class="form-group">
    {!! Form::label('nohp', 'Nohp:') !!}
    <p>{!! $users->nohp !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $users->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $users->updated_at !!}</p>
</div>

