@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Modul Rencana
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($modulRencana, ['route' => ['modulRencanas.update', $modulRencana->id], 'method' => 'patch']) !!}

                        @include('modul_rencanas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection