<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulRencanasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modul_rencanas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('NIK');
            $table->string('tahun_ajar');
            $table->string('prodi_fk');
            $table->string('bidang');
            $table->string('sub_bidang');
            $table->string('nama_kegiatan');
            $table->string('rencana_sks');
            $table->string('keterangan')->nullable();
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modul_rencanas');
    }
}
