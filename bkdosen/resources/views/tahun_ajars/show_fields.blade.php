<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tahunAjar->id !!}</p>
</div>

<!-- Tahun Ajar Field -->
<div class="form-group">
    {!! Form::label('tahun_ajar', 'Tahun Ajar:') !!}
    <p>{!! $tahunAjar->tahun_ajar !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tahunAjar->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tahunAjar->updated_at !!}</p>
</div>

