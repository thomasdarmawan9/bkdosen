<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'NIK' => '20162016',
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('123123'),
            'jabatan' => 'Admin',
            'prodi' => '',
            'nohp' => '08123456789',
        ]);
    }
}
