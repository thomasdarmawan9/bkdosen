<!-- Nik Field -->
<div class="form-group col-sm-6">
    {!! Form::label('NIK', 'Nik:') !!}
    {!! Form::text('NIK', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Jabatan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jabatan', 'Jabatan:') !!}
    {!! Form::select('jabatan', array(''=>'Pilih Jabatan', 'Admin' => 'Admin', 'Dekan' => 'Dekan', 'Kaprodi' => 'Kaprodi',  'Dosen' => 'Dosen', 'HRD' => 'HRD', 'LPMU' => 'LPMU', 'LPPM' => 'LPPM'), 'Pilih Jabatan', ['class' => 'form-control', 'id' => 'roles']); !!}
</div>

<!-- Prodi Field -->
<div class="form-group col-sm-6" id="prodi">
    {!! Form::label('prodi', 'Prodi:') !!}
     {!! Form::select('prodi', array('Akuntansi' => 'Akuntansi', 'Manajemen' => 'Manajemen', 'Ilmu Komunikasi' => 'Ilmu Komunikasi', 'Psikologi' => 'Psikologi', 'Desain Komunikasi Visual' => 'Desain Komunikasi Visual', 'Desain Produk' => 'Desain Produk', 'Informatika' => 'Informatika', 'Sistem Informasi' => 'Sistem Informasi', 'Teknik Sipil' => 'Teknik Sipil', 'Arsitektur' => 'Arsitektur'), 'Akuntansi', ['class' => 'form-control']); !!}
</div>


<!-- Nohp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nohp', 'Nohp:') !!}
    {!! Form::text('nohp', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
</div>
