<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createmodul_rencanaAPIRequest;
use App\Http\Requests\API\Updatemodul_rencanaAPIRequest;
use App\Models\modul_rencana;
use App\Repositories\modul_rencanaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class modul_rencanaController
 * @package App\Http\Controllers\API
 */

class modul_rencanaAPIController extends AppBaseController
{
    /** @var  modul_rencanaRepository */
    private $modulRencanaRepository;

    public function __construct(modul_rencanaRepository $modulRencanaRepo)
    {
        $this->modulRencanaRepository = $modulRencanaRepo;
    }

    /**
     * Display a listing of the modul_rencana.
     * GET|HEAD /modulRencanas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->modulRencanaRepository->pushCriteria(new RequestCriteria($request));
        $this->modulRencanaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $modulRencanas = $this->modulRencanaRepository->all();

        return $this->sendResponse($modulRencanas->toArray(), 'Modul Rencanas retrieved successfully');
    }

    /**
     * Store a newly created modul_rencana in storage.
     * POST /modulRencanas
     *
     * @param Createmodul_rencanaAPIRequest $request
     *
     * @return Response
     */
    public function store(Createmodul_rencanaAPIRequest $request)
    {
        $input = $request->all();

        $modulRencanas = $this->modulRencanaRepository->create($input);

        return $this->sendResponse($modulRencanas->toArray(), 'Modul Rencana saved successfully');
    }

    /**
     * Display the specified modul_rencana.
     * GET|HEAD /modulRencanas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var modul_rencana $modulRencana */
        $modulRencana = $this->modulRencanaRepository->findWithoutFail($id);

        if (empty($modulRencana)) {
            return $this->sendError('Modul Rencana not found');
        }

        return $this->sendResponse($modulRencana->toArray(), 'Modul Rencana retrieved successfully');
    }

    /**
     * Update the specified modul_rencana in storage.
     * PUT/PATCH /modulRencanas/{id}
     *
     * @param  int $id
     * @param Updatemodul_rencanaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatemodul_rencanaAPIRequest $request)
    {
        $input = $request->all();

        /** @var modul_rencana $modulRencana */
        $modulRencana = $this->modulRencanaRepository->findWithoutFail($id);

        if (empty($modulRencana)) {
            return $this->sendError('Modul Rencana not found');
        }

        $modulRencana = $this->modulRencanaRepository->update($input, $id);

        return $this->sendResponse($modulRencana->toArray(), 'modul_rencana updated successfully');
    }

    /**
     * Remove the specified modul_rencana from storage.
     * DELETE /modulRencanas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var modul_rencana $modulRencana */
        $modulRencana = $this->modulRencanaRepository->findWithoutFail($id);

        if (empty($modulRencana)) {
            return $this->sendError('Modul Rencana not found');
        }

        $modulRencana->delete();

        return $this->sendResponse($id, 'Modul Rencana deleted successfully');
    }
}
