
<div class="form-group col-sm-6">
    {!! Form::label('id_rencana_fk', 'id rencana:') !!}
    {!! Form::select('id_rencana_fk', $id_rencana , null ,['class' => 'form-control']) !!}
 
</div>
<div class="form-group col-sm-6">
    {!! Form::label('realisasi_sks', 'Realisasi SKS:') !!}
    {!! Form::text('realisasi_sks', null , ['class' => 'form-control']) !!}
 
</div>
<div class="form-group col-sm-6">
    {!! Form::label('bukti_pelaksanaan_sk', 'Bukti Pelaksanaan SK:') !!}
    {!! Form::file('bukti_pelaksanaan_sk', null , ['class' => 'form-control']) !!}
 
</div>
<div class="form-group col-sm-6">
    {!! Form::label('bukti_pelaksanaan_presensi', 'Bukti Pelaksanaan Presensi:') !!}
    {!! Form::file('bukti_pelaksanaan_presensi', null , ['class' => 'form-control']) !!}
 
</div>
<div class="form-group col-sm-6">
    {!! Form::label('bukti_pelaksanaan_ppt', 'Bukti Pelaksanaan PPT:') !!}
    {!! Form::file('bukti_pelaksanaan_ppt', null , ['class' => 'form-control']) !!}
 
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('realisasis.index') !!}" class="btn btn-default">Cancel</a>
</div>
