@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tahun Ajar
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('tahun_ajars.show_fields')
                    <a href="{!! route('tahunAjars.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
