<?php

namespace App\Repositories;

use App\Models\modul_rencana;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class modul_rencanaRepository
 * @package App\Repositories
 * @version March 7, 2019, 10:39 am UTC
 *
 * @method modul_rencana findWithoutFail($id, $columns = ['*'])
 * @method modul_rencana find($id, $columns = ['*'])
 * @method modul_rencana first($columns = ['*'])
*/
class modul_rencanaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'NIK',
        'tahun_ajar',
        'prodi_fk',
        'bidang',
        'sub_bidang',
        'nama_kegiatan',
        'rencana_sks',
        'keterangan',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return modul_rencana::class;
    }
}
