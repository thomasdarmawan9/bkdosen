<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreaterealisasiAPIRequest;
use App\Http\Requests\API\UpdaterealisasiAPIRequest;
use App\Models\realisasi;
use App\Repositories\realisasiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class realisasiController
 * @package App\Http\Controllers\API
 */

class realisasiAPIController extends AppBaseController
{
    /** @var  realisasiRepository */
    private $realisasiRepository;

    public function __construct(realisasiRepository $realisasiRepo)
    {
        $this->realisasiRepository = $realisasiRepo;
    }

    /**
     * Display a listing of the realisasi.
     * GET|HEAD /realisasis
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->realisasiRepository->pushCriteria(new RequestCriteria($request));
        $this->realisasiRepository->pushCriteria(new LimitOffsetCriteria($request));
        $realisasis = $this->realisasiRepository->all();

        return $this->sendResponse($realisasis->toArray(), 'Realisasis retrieved successfully');
    }

    /**
     * Store a newly created realisasi in storage.
     * POST /realisasis
     *
     * @param CreaterealisasiAPIRequest $request
     *
     * @return Response
     */
    public function store(CreaterealisasiAPIRequest $request)
    {
        $input = $request->all();

        $realisasi = $this->realisasiRepository->create($input);

        return $this->sendResponse($realisasi->toArray(), 'Realisasi saved successfully');
    }

    /**
     * Display the specified realisasi.
     * GET|HEAD /realisasis/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var realisasi $realisasi */
        $realisasi = $this->realisasiRepository->findWithoutFail($id);

        if (empty($realisasi)) {
            return $this->sendError('Realisasi not found');
        }

        return $this->sendResponse($realisasi->toArray(), 'Realisasi retrieved successfully');
    }

    /**
     * Update the specified realisasi in storage.
     * PUT/PATCH /realisasis/{id}
     *
     * @param  int $id
     * @param UpdaterealisasiAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdaterealisasiAPIRequest $request)
    {
        $input = $request->all();

        /** @var realisasi $realisasi */
        $realisasi = $this->realisasiRepository->findWithoutFail($id);

        if (empty($realisasi)) {
            return $this->sendError('Realisasi not found');
        }

        $realisasi = $this->realisasiRepository->update($input, $id);

        return $this->sendResponse($realisasi->toArray(), 'realisasi updated successfully');
    }

    /**
     * Remove the specified realisasi from storage.
     * DELETE /realisasis/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var realisasi $realisasi */
        $realisasi = $this->realisasiRepository->findWithoutFail($id);

        if (empty($realisasi)) {
            return $this->sendError('Realisasi not found');
        }

        $realisasi->delete();

        return $this->sendResponse($id, 'Realisasi deleted successfully');
    }
}
