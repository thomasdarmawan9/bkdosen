<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/modulVerif', 'modul_rencanaController@index2');

Route::resource('modulRencanas', 'modul_rencanaController');

Route::resource('users', 'usersController');

Route::get('/verifikasi/{id_rencana}', 'modul_rencanaController@verifikasi');


Route::resource('tahunAjars', 'tahun_ajarController');

Route::resource('realisasis', 'realisasiController');

Route::post('/ubah_file', 'realisasiController@ubah_file');





