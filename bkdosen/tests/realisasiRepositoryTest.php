<?php namespace Tests\Repositories;

use App\Models\realisasi;
use App\Repositories\realisasiRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakerealisasiTrait;
use Tests\ApiTestTrait;

class realisasiRepositoryTest extends TestCase
{
    use MakerealisasiTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var realisasiRepository
     */
    protected $realisasiRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->realisasiRepo = \App::make(realisasiRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_realisasi()
    {
        $realisasi = $this->fakerealisasiData();
        $createdrealisasi = $this->realisasiRepo->create($realisasi);
        $createdrealisasi = $createdrealisasi->toArray();
        $this->assertArrayHasKey('id', $createdrealisasi);
        $this->assertNotNull($createdrealisasi['id'], 'Created realisasi must have id specified');
        $this->assertNotNull(realisasi::find($createdrealisasi['id']), 'realisasi with given id must be in DB');
        $this->assertModelData($realisasi, $createdrealisasi);
    }

    /**
     * @test read
     */
    public function test_read_realisasi()
    {
        $realisasi = $this->makerealisasi();
        $dbrealisasi = $this->realisasiRepo->find($realisasi->id);
        $dbrealisasi = $dbrealisasi->toArray();
        $this->assertModelData($realisasi->toArray(), $dbrealisasi);
    }

    /**
     * @test update
     */
    public function test_update_realisasi()
    {
        $realisasi = $this->makerealisasi();
        $fakerealisasi = $this->fakerealisasiData();
        $updatedrealisasi = $this->realisasiRepo->update($fakerealisasi, $realisasi->id);
        $this->assertModelData($fakerealisasi, $updatedrealisasi->toArray());
        $dbrealisasi = $this->realisasiRepo->find($realisasi->id);
        $this->assertModelData($fakerealisasi, $dbrealisasi->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_realisasi()
    {
        $realisasi = $this->makerealisasi();
        $resp = $this->realisasiRepo->delete($realisasi->id);
        $this->assertTrue($resp);
        $this->assertNull(realisasi::find($realisasi->id), 'realisasi should not exist in DB');
    }
}
