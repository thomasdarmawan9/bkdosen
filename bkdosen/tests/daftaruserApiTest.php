<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class daftaruserApiTest extends TestCase
{
    use MakedaftaruserTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatedaftaruser()
    {
        $daftaruser = $this->fakedaftaruserData();
        $this->json('POST', '/api/v1/daftarusers', $daftaruser);

        $this->assertApiResponse($daftaruser);
    }

    /**
     * @test
     */
    public function testReaddaftaruser()
    {
        $daftaruser = $this->makedaftaruser();
        $this->json('GET', '/api/v1/daftarusers/'.$daftaruser->id);

        $this->assertApiResponse($daftaruser->toArray());
    }

    /**
     * @test
     */
    public function testUpdatedaftaruser()
    {
        $daftaruser = $this->makedaftaruser();
        $editeddaftaruser = $this->fakedaftaruserData();

        $this->json('PUT', '/api/v1/daftarusers/'.$daftaruser->id, $editeddaftaruser);

        $this->assertApiResponse($editeddaftaruser);
    }

    /**
     * @test
     */
    public function testDeletedaftaruser()
    {
        $daftaruser = $this->makedaftaruser();
        $this->json('DELETE', '/api/v1/daftarusers/'.$daftaruser->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/daftarusers/'.$daftaruser->id);

        $this->assertResponseStatus(404);
    }
}
