<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakerealisasiTrait;
use Tests\ApiTestTrait;

class realisasiApiTest extends TestCase
{
    use MakerealisasiTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_realisasi()
    {
        $realisasi = $this->fakerealisasiData();
        $this->response = $this->json('POST', '/api/realisasis', $realisasi);

        $this->assertApiResponse($realisasi);
    }

    /**
     * @test
     */
    public function test_read_realisasi()
    {
        $realisasi = $this->makerealisasi();
        $this->response = $this->json('GET', '/api/realisasis/'.$realisasi->id);

        $this->assertApiResponse($realisasi->toArray());
    }

    /**
     * @test
     */
    public function test_update_realisasi()
    {
        $realisasi = $this->makerealisasi();
        $editedrealisasi = $this->fakerealisasiData();

        $this->response = $this->json('PUT', '/api/realisasis/'.$realisasi->id, $editedrealisasi);

        $this->assertApiResponse($editedrealisasi);
    }

    /**
     * @test
     */
    public function test_delete_realisasi()
    {
        $realisasi = $this->makerealisasi();
        $this->response = $this->json('DELETE', '/api/realisasis/'.$realisasi->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/realisasis/'.$realisasi->id);

        $this->response->assertStatus(404);
    }
}
