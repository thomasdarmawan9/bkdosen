<?php

use App\Models\modul_rencana;
use App\Repositories\modul_rencanaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class modul_rencanaRepositoryTest extends TestCase
{
    use Makemodul_rencanaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var modul_rencanaRepository
     */
    protected $modulRencanaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->modulRencanaRepo = App::make(modul_rencanaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatemodul_rencana()
    {
        $modulRencana = $this->fakemodul_rencanaData();
        $createdmodul_rencana = $this->modulRencanaRepo->create($modulRencana);
        $createdmodul_rencana = $createdmodul_rencana->toArray();
        $this->assertArrayHasKey('id', $createdmodul_rencana);
        $this->assertNotNull($createdmodul_rencana['id'], 'Created modul_rencana must have id specified');
        $this->assertNotNull(modul_rencana::find($createdmodul_rencana['id']), 'modul_rencana with given id must be in DB');
        $this->assertModelData($modulRencana, $createdmodul_rencana);
    }

    /**
     * @test read
     */
    public function testReadmodul_rencana()
    {
        $modulRencana = $this->makemodul_rencana();
        $dbmodul_rencana = $this->modulRencanaRepo->find($modulRencana->id);
        $dbmodul_rencana = $dbmodul_rencana->toArray();
        $this->assertModelData($modulRencana->toArray(), $dbmodul_rencana);
    }

    /**
     * @test update
     */
    public function testUpdatemodul_rencana()
    {
        $modulRencana = $this->makemodul_rencana();
        $fakemodul_rencana = $this->fakemodul_rencanaData();
        $updatedmodul_rencana = $this->modulRencanaRepo->update($fakemodul_rencana, $modulRencana->id);
        $this->assertModelData($fakemodul_rencana, $updatedmodul_rencana->toArray());
        $dbmodul_rencana = $this->modulRencanaRepo->find($modulRencana->id);
        $this->assertModelData($fakemodul_rencana, $dbmodul_rencana->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletemodul_rencana()
    {
        $modulRencana = $this->makemodul_rencana();
        $resp = $this->modulRencanaRepo->delete($modulRencana->id);
        $this->assertTrue($resp);
        $this->assertNull(modul_rencana::find($modulRencana->id), 'modul_rencana should not exist in DB');
    }
}
