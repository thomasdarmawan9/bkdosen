<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreaterealisasiRequest;
use App\Http\Requests\UpdaterealisasiRequest;
use App\Repositories\realisasiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class realisasiController extends AppBaseController
{
    /** @var  realisasiRepository */
    private $realisasiRepository;

    public function __construct(realisasiRepository $realisasiRepo)
    {
        $this->realisasiRepository = $realisasiRepo;
    }

    /**
     * Display a listing of the realisasi.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->realisasiRepository->pushCriteria(new RequestCriteria($request));
        $realisasis = $this->realisasiRepository->with('rencana')->all();

        return view('realisasis.index')
            ->with('realisasis', $realisasis);
    }

    /**
     * Show the form for creating a new realisasi.
     *
     * @return Response
     */
    public function create()
    {
        $id_rencana = DB::table('modul_rencanas')->where('status','=','Verified')->pluck('id','id');

        return view('realisasis.create', compact('id_rencana','id_rencana'));
    }

    /**
     * Store a newly created realisasi in storage.
     *
     * @param CreaterealisasiRequest $request
     *
     * @return Response
     */
    public function store(CreaterealisasiRequest $request)
    {
        $input = $request->all();
        echo "<pre>";
        print_r($input);
        $bukti_pelaksanaan_sk = $request->input('bukti_pelaksanaan_sk');
        $input['bukti_pelaksanaan_sk'] = md5($request->file('bukti_pelaksanaan_sk')->getClientOriginalName().time()).'.'.$request->file('bukti_pelaksanaan_sk')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('bukti_pelaksanaan_sk')->move($destination, $input['bukti_pelaksanaan_sk']);

        $bukti_pelaksanaan_presensi = $request->input('bukti_pelaksanaan_presensi');
        $input['bukti_pelaksanaan_presensi'] = md5($request->file('bukti_pelaksanaan_presensi')->getClientOriginalName().time()).'.'.$request->file('bukti_pelaksanaan_presensi')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('bukti_pelaksanaan_presensi')->move($destination, $input['bukti_pelaksanaan_presensi']);

        $bukti_pelaksanaan_ppt = $request->input('bukti_pelaksanaan_ppt');
        $input['bukti_pelaksanaan_ppt'] = md5($request->file('bukti_pelaksanaan_ppt')->getClientOriginalName().time()).'.'.$request->file('bukti_pelaksanaan_ppt')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('bukti_pelaksanaan_ppt')->move($destination, $input['bukti_pelaksanaan_ppt']);

        

        $realisasi = $this->realisasiRepository->create($input);

        Flash::success('Realisasi saved successfully.');

        return redirect(route('realisasis.index'));

        
    }

    /**
     * Display the specified realisasi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $realisasi = $this->realisasiRepository->findWithoutFail($id);

        if (empty($realisasi)) {
            Flash::error('Realisasi not found');

            return redirect(route('realisasis.index'));
        }

        return view('realisasis.show')->with('realisasi', $realisasi);
    }

    /**
     * Show the form for editing the specified realisasi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $realisasi = $this->realisasiRepository->findWithoutFail($id);

        if (empty($realisasi)) {
            Flash::error('Realisasi not found');

            return redirect(route('realisasis.index'));
        }

        return view('realisasis.edit')->with('realisasi', $realisasi);
    }

    /**
     * Update the specified realisasi in storage.
     *
     * @param  int              $id
     * @param UpdaterealisasiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdaterealisasiRequest $request)
    {
        $realisasi = $this->realisasiRepository->findWithoutFail($id);

        if (empty($realisasi)) {
            Flash::error('Realisasi not found');

            return redirect(route('realisasis.index'));
        }

        $realisasi = $this->realisasiRepository->update($request->all(), $id);

        Flash::success('Realisasi updated successfully.');

        return redirect(route('realisasis.index'));
    }

    /**
     * Remove the specified realisasi from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $realisasi = $this->realisasiRepository->findWithoutFail($id);

        if (empty($realisasi)) {
            Flash::error('Realisasi not found');

            return redirect(route('realisasis.index'));
        }

        $this->realisasiRepository->delete($id);

        Flash::success('Realisasi deleted successfully.');

        return redirect(route('realisasis.index'));
    }
    public function ubah($id){
        $realisasi = Realisasi::find($id);
        return view('realisasis.edit', ['realisasi' => $realisasi]);
    }
    public function ubah_file($id){
        
 
		return redirect()->back();
    }

  
}
